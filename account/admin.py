from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import UserBase

class UserAdmin(BaseUserAdmin):

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.

    list_display = ('id', 'email',  'first_name', 'country', 'phone_number',
        'postcode', 'address_line_1', 'town_city', 'is_active', 'is_staff', 'created', 
        'updated')
    list_filter = ('is_staff',)
    fieldsets = (
        (None, {'fields': ('email',)}),
        ('First name', {'fields': ('first_name',)}),
        ('Country', {'fields': ('country',)}),
        ('Phone number', {'fields': ('phone_number',)}),
        ('Postcode', {'fields': ('postcode',)}),
        ('Address line 1', {'fields': ('address_line_1',)}),
        ('Town city', {'fields': ('town_city',)}),
        ('is_active', {'fields': ('is_active',)})
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'first_name', 'country', 'password1', 'password2'),
        }),
    )
    search_fields = ('email',)
    ordering = ('email',)

admin.site.register(UserBase, UserAdmin)
