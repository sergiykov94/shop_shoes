from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMessage

from shop.settings import FROM_EMAIL, EMAIL_ADMIN

class SendingEmail(object):
    """
        Class send message to users
    """
    from_email = "Sk shop shoes <%s>" % FROM_EMAIL
    target_emails = []
    # A list or tuple of addresses used in the “Bcc” header when sending the email.
    bcc_emails = [] 

    def sending_email(self, subject, message, email=None, ):

        if not email:
            email = EMAIL_ADMIN

        target_emails = [email]

        msg = EmailMessage(subject, message, from_email=self.from_email, 
            to = target_emails, bcc = self.bcc_emails,
            )
        msg.content_subtype = 'html'
        msg.mixed_subtype = 'related'
        msg.send()
