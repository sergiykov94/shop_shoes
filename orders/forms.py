from django import forms
from .models import Order


class OrderCreateForm(forms.Form):
    first_name = forms.CharField(max_length = 25)
    email = forms.EmailField(max_length = 35)
    phone_number = forms.CharField(max_length = 35)
    comment = forms.CharField(max_length=255 )
