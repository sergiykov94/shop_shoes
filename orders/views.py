from django.shortcuts import render
from .models import OrderItem, Order
from .forms import OrderCreateForm
from basket.basket import Basket

# Create your views here.

def order_create(request):
    """
        Function should create order and we can send email to customer
        and to shop manager
    """
    basket = Basket(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            data = request.POST
            first_name = data.get('first_name')
            email = data.get('email')
            phone_number = data.get('phone_number')
            comment = data.get('comment')
            order = Order.objects.create(
                first_name = first_name,
                email = email,
                phone_number = phone_number,
                comment = comment,
            )
            if basket.coupon:
                order.coupon = basket.coupon
                order.discount = basket.coupon.discount
            order.save()
            for item in basket:
                OrderItem.objects.create(order = order,
                                        product = item['product'],
                                        price = item['price'],
                                        quantity = item['qty'])
            # email = SendingEmail()
            # email.sending_email(type_id = 1, order = order)
            # email.sending_email(type_id = 2, email = order.email, order = order)
            basket.clear()
            return render(request, 'order/thank_you.html')
    else:
        form = OrderCreateForm
    return render(request, 'order/order.html', 
                    {'basket' : basket, 'form' : form })


