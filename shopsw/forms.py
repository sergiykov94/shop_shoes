from django import forms


class QuestionForm(forms.Form):
    """
        Question form
    """
    username = forms.CharField(max_length = 100)
    email = forms.EmailField(max_length = 45)
    telephone = forms.CharField(max_length = 30)
    comment = forms.CharField(widget=forms.Textarea, max_length = 255)
