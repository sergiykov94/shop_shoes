from django.contrib import admin
from .models import Category, Product, Slider, Question


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}


class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'price', 'stock', 'available', 'created', 'updated',]
    list_filter = ['available', 'created', 'updated']
    list_editable = ['price', 'stock', 'available']
    prepopulated_fields = {'slug': ('name',)}

class SliderAdmin(admin.ModelAdmin):
    list_display = ['name', 'image', 'description', 'created', 'available',]
    list_editable = ['available']

class QuestionAdmin(admin.ModelAdmin):
    list_display = ['username', 'email', 'phone', 'comment', 'created',]

admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Slider, SliderAdmin)
admin.site.register(Question, QuestionAdmin)
