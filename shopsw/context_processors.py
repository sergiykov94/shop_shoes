from .models import Category, Slider

def categories(request):
    return {
    'categories' : Category.objects.all()
    }

def slider(request):
    return { 
    'sliders' : Slider.objects.filter(available = True)
    }
