from django.shortcuts import render, get_object_or_404, redirect
from .models import Category, Product, Slider, Question
from django.core.paginator import Paginator
from .forms import QuestionForm
from coupons.forms import CouponApplyForm


def product_list(request, category_slug=None):
    """
        Function should get prouct category and products
    """

    category = None
    products = Product.objects.filter(available=True).all()
    paginator = Paginator(products, 6)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    # import pdb; pdb.set_trace()
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
        paginator = Paginator(products, 6)
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
    return render(request,
                  'index.html',
                  {'category': category,
                   'products': products,
                   'page_obj': page_obj})

def product_detail(request, slug):
    """
        Function should show product detail
    """

    product = get_object_or_404(Product,
                                slug=slug,
                                available=True)
    return render(request,
                  'productdetail.html',
                  {'product': product,
                  })

def about_us(request):

    return render(request, 'about.html')

def faqs(request):

    return render(request, 'faqs.html')


def contact(request):
    """
      Function should create question form
    """
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            username = data['username']
            phone = data['telephone']
            email = data['email']
            comment = data['comment']
            question = Question.objects.create(
                username = username,
                email = email,
                phone = phone,
                comment = comment,
                )
            return render(request, 'order/thank_you.html')
    else:
        form = QuestionForm
    return render(request, 'contact.html')

