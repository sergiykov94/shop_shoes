from django.conf.urls import url
from . import views
from django.urls import path

app_name = 'shopsw'

urlpatterns = [
    path('', views.product_list, name='product_list'),
    path('category/<slug:category_slug>', views.product_list, name='category_list'),
    path('<slug:slug>', views.product_detail, name='product_detail'),
    path('about_us/', views.about_us, name='about_us'),
    path('faqs/', views.faqs, name='faqs'),
    path('contact/', views.contact, name='contact'),
]
