from django.urls import path
from . import views

app_name = 'coupons'

urlpatterns = [
    path('accept/', views.coupon_apply, name='accept'),

]