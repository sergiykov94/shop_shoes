from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone
from django.views.decorators.http import require_POST
from .models import Coupon
from .forms import CouponApplyForm


@require_POST
def coupon_apply(request):
    """
        Function should apply coupon to order 
    """
    now = timezone.now()
    form = CouponApplyForm(request.POST)
    if form.is_valid():
        code = form.cleaned_data['code']
        try:
            coupon = Coupon.objects.get(code__iexact=code, 
                                        valid_from__lte=now, 
                                        valid_to__gte=now, 
                                        active=True)
            request.session['coupon_id'] = coupon.id
        except Coupon.DoesNotExist: 
            request.session['coupon_id'] = None
        # import pdb; pdb.set_trace()
    return redirect('basket:basket_summary')

