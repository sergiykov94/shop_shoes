from django import forms

class CouponApplyForm(forms.Form):

    current_url = forms.CharField(max_length = 100)

    code = forms.CharField()