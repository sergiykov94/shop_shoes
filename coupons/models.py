from django.db import models
import secrets
from django.db.models.signals import post_save
from django.core.validators import MinValueValidator, MaxValueValidator

class Coupon(models.Model):

    code = models.CharField(max_length=8, blank=True, null=True, unique=True)
    valid_from = models.DateTimeField(auto_now_add=True)
    valid_to = models.DateTimeField(
        help_text='<small style="color:red"> Сoupon valid to </small>'
    )
    discount = models.IntegerField(
                      validators=[MinValueValidator(0),
                                  MaxValueValidator(100)])

    active = models.BooleanField()

    def __str__(self):
        return self.code


    @classmethod
    def post_create(cls, sender, instance, created, *args, **kwargs):
        """
            Function should create random coupon code
        """
        if created:
            id_string = str(instance.id)
            upper_alpha = "ABCDEFGHJKLMNPQRSTVWXYZ"
            random_str = "".join(secrets.choice(upper_alpha) for i in range(8))
            instance.code = (random_str + id_string)[-8:]
            instance.save()

post_save.connect(Coupon.post_create, sender=Coupon)