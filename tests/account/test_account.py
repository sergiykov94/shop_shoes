"""
    Test user module
"""

import pytest

@pytest.mark.django_db
class TestAccount:


    def test_user_create(self, user_model):
        """
            Test should create user in database
        """
        user_model.objects.create_user('some_user', 'some_email@gmail.com', 'password')
        user_in_db = user_model.objects.all()
        assert len(user_in_db) == 1
    
    def test_should_registration_user(self, client, user_model):
        """
            Test should registration user in database
        """
        # activate = False
        payload = {
            'username' : 'some_user',
            'email' : 'some_email@gmail.com',
            'password' : '12345',
            'password2' : '12345',
            'is_active' : False,
        }
        response = client.post(
            '/en/account/register/',
            data = payload,
            )
        assert response.status_code == 200
        user_in_db = user_model.objects.all()
        assert len(user_in_db) == 1
        # assert user_in_db.is_active == False
        # import pdb; pdb.set_trace()


