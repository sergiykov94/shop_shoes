"""
    Test user module
"""

import pytest

@pytest.mark.django_db
class TestAccount:

    def test_should_add_product_to_basket(self, client):
        """
            Test should add product in basket
        """
        payload = {
            'product_id' : int(1),
            'product_qty' : int(2),
        }
        response = client.post(
            '/en/basket/add/',
            )
        data = response.json()
        assert response.status_code == 200
