"""
    Conftest
"""


import pytest
from functools import partial
from account.models import UserBase


@pytest.fixture
def employee_user():
    user = UserBase.objects.create_user("some_user@mail.com", "password")
    user.save()
    return user

@pytest.fixture
def activator_account():
    return 'account/activate/Mzg/am64s3-949c175a679d67962c8a8679dd636204)'



@pytest.fixture
def user_model():
    return UserBase
